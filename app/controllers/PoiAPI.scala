package controllers

import scala.concurrent.Future
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json._
import play.api.libs.json.Reads._
import play.api.mvc.Action
import play.api.mvc.Controller
import play.api.mvc.SimpleResult
import play.Logger
import play.api.libs.ws.WS

object PoiAPI extends Controller {

    val END_POINT = "https://maps.googleapis.com/maps/api/place/textsearch/json?";
    val API_KEY = "AIzaSyBsp82AFb_USZhlW1fSIJwLLlaCeSfLxEk";

    def poiQuery(q: String) = Action.async {
        q match {
            case "" =>
                val json: JsValue = Json.parse("""
                          {
                            "status" : "ZERO_RESULTS",
                            "results" : []
                          }
                          """)

                val futureResult: Future[SimpleResult] = scala.concurrent.Future {
                    Ok(json)
                }
                futureResult

            case _ =>
                Logger.debug(END_POINT + "query=" + q + "&key=" + API_KEY
                    + "&sensor=false&language=zh-TW")

                WS.url(END_POINT)
                    .withQueryString(("query", q), ("key", API_KEY), ("senor", "false"), ("language", "zh-TW"))
                    .get()
                    .map { response =>
                        Logger.debug("status code: " + response.status);
                        Logger.debug("body: " + response.body);

                        response.status match {
                            case 200 =>
                                val resJson = response.json

                                (resJson \ "status").as[String] match {
                                    case "OK" =>
                                        val ezResult: JsValue = Json.obj(
                                            "source" -> "google",
                                            "status" -> resJson \ "status",
                                            "html_attributions" -> resJson \ "html_attributions",
                                            "results" -> (resJson \ "results").as[List[JsObject]]
                                                .map(item =>
                                                    Json.obj(
                                                        "place_id" -> item \ "place_id",
                                                        "name" -> item \ "name",
                                                        "formatted_address" -> item \ "formatted_address",
                                                        "loc" -> JsObject(Seq(
                                                            "type" -> JsString("Point"),
                                                            "coordinates" -> JsArray(Seq(
                                                                item \ "geometry" \ "location" \ "lat",
                                                                item \ "geometry" \ "location" \ "lng")))),
                                                        "rating" -> item \ "rating",
                                                        "types" -> item \ "types",
                                                        "photos" -> item \ "photos"
                                                    )).map(item =>
                                                    JsObject(item.fields.filterNot(f =>
                                                        f._2.isInstanceOf[JsUndefined]
                                                    )))
                                        )
                                        //Logger.debug(ezResult.toString)
                                        Ok(ezResult)
                                    case _ =>
                                        val ezResult: JsValue = JsObject(Json.obj(
                                            "source" -> "google",
                                            "status" -> resJson \ "status",
                                            "html_attributions" -> resJson \ "html_attributions",
                                            "error_message" -> resJson \ "error_message"
                                        ).fields.filterNot(f => f._2.isInstanceOf[JsUndefined]))

                                        Ok(ezResult)
                                }
                            case _ => {
                                NotFound("WS Req: Error 404 (Not Found)!")
                            }
                        }
                    } // END WS map()
        }
    }
}