package controllers

import play.api._
import play.api.mvc._
import play.api.cache._
import play.api.Play.current

object Application extends Controller {

  def index = Action {
      
    Logger.debug("gogogo")
    var a = 0
    for(a <- 2 to 100) {
      Cache.set("key" + System.currentTimeMillis() , System.currentTimeMillis() + "")
    }
    
    Ok(views.html.index())
  }

  def poiList = Action {
    Ok(views.html.poi_list())
  }

  def poiQuery = Action {
    Ok(views.html.poi_query())
  }

}